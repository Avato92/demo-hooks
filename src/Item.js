import { memo, useEffect } from "react";

export const Item = memo(({ user, handleDelete }) => {
  useEffect(() => {
    console.log("Item render " + user.name);
  });

  return (
    <li>
      {user.name}
      <button onClick={() => handleDelete(user.id)}>Delete</button>
    </li>
  );
});

export default Item;
