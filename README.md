# How to use memo, useMemo and useCallback

## First step

There is a simple app with a add button and a list of items

## Problem to fix

When app started, there are 4 console.log with "render X", due to a initial app render.

If change the text into the input, observo how the console shows 4 console.log again.

This is the problem to solve.

### memo

The first solution is simple, add memo to the component List and it doesn't re-render if props don't change.

## Second problem to fix

Now, the component List and Item don't re-render if the state of the input change, but what happen if add a new user?

### memo again

The solution is use memo again on the Item component.

### Can I use memo in all components?

No.

### Why?

Because, if component is very soft, probably the perfomance to memorize the state and the component is hardest than re-render the component one more time.

### When I need to use memo?

When the component has a lot of data or when the component fetch to an API

## Third problem to fix

See the console, if declare a function into the component, all the times the component is re-rendered, the function is re-declared

### Why is that a problem?

Because, imagine this function isn't a little function and you have a big array with its methods, this is a big problem

### How to solve it?

To try to fix this problem is necessary to use useMemo, this hook needs two parameters, an arrow function with the main function and as useEffect hook, the variables who trigger this hook.

### When I could use useMemo?

When you have a functions with a big process.

## Fourth problem

Now, the app have a new delete function, created at App and the component Item receives it by props.

The app works great, but what happen if I write in the input text?

All the components are re-rendered again?

### Why?

Because, if I create a function and send it by props, when the component which has this function re-create this function in another piece of memory and the child components think is a different prop and force re-render.

### useCallback

The solution is use the hook useCallback, this hook needs the function to memorize and the variables to trigger and re-create the function.

This function memorize all the state of the function in the moment when it's create.

## Resume

### memo

- Memorize a complete component.
- Re-memorize when props are changed.
- Avoid re-renders.

#### When use this function?

- When have a big list with a lot of data and it costs more re-render than memorize this component.
- When call an API.

### useMemo

- Memorize a calculated data.

#### When use this function?

- Heavy process with a lot of loops or interactions.

### useCallback

- Memorize a function to avoid re-definition in all renders.

### When use this function?

- When use a function as an effect.
- When pass a function as a parameter into a child component and memorize. (If don't do this, memo function is useless)